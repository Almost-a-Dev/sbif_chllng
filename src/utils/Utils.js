const toNumberArr = (arr) => {
    return arr.map(d => parseInt(d.Valor.replace(/,/g, '.'), 10))
}

const getTimezone = () => {
    return new Date().toLocaleString("es-ES", { timeZone: "America/Santiago" });
}

const defaultDate = () => {
    const startDate = splitDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000).toLocaleString("es-ES", { timeZone: "America/Santiago" }));
    const endDate = splitDate(getTimezone());
    return {
        startDate,
        endDate
    }
}

const buildDateURL = (date = null) => {
    let url = '';
    if (date !== null) {
        const [sd, sm, sy] = splitDate(date.startDate);
        const [ed, em, ey] = splitDate(date.endDate);
        url = `${sy}/${sm}/dias_i/${sd}/${ey}/${em}/dias_f/${ed}`
    } else {
        const [sd, sm, sy] = defaultDate().startDate;
        const [ed, em, ey] = defaultDate().endDate;
        url = `${sy}/${sm}/dias_i/${sd}/${ey}/${em}/dias_f/${ed}`
    }
    return url;
}

const splitDate = (dt) => {
    let newDate = ''
    if (dt.includes(' ')) {
        newDate = dt.split(' ')[0].split('/')
    } else {
        newDate = dt.split('-').reverse()
    }
    return newDate;
}

const getMax = (arr) => {
    const arrOfNums = toNumberArr(arr)
    return Math.max(...arrOfNums);
}

const getMin = (arr) => {
    const arrOfNums = toNumberArr(arr)
    return Math.min(...arrOfNums);
}

const getAvg = (arr) => {
    const arrOfNums = toNumberArr(arr)
    return parseInt(arrOfNums.reduce((d, acc) => d + acc, 0) / arrOfNums.length)
}

const getDayMonthYear = (str) => {
    return str.split('-');
}

export {
    getMax,
    getMin,
    getAvg,
    getDayMonthYear,
    splitDate,
    getTimezone,
    defaultDate,
    buildDateURL
};