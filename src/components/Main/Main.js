import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from '../Datepicker/DatePicker';
import Chart2 from '../Chart/Chart2';
import Display from '../Display/Display';
import Error from '../../shared/Errorhand/Error';
import Spinner from '../../shared/Spinner/Spinner';

import { IconContext } from "react-icons";
import { RiMoneyDollarCircleLine } from 'react-icons/ri';

import { getMax, getMin, getAvg, buildDateURL } from '../../utils/Utils';

import './Main.css'

class Main extends Component {

    constructor(props) {
        super(props)
        this.state = {
            usd: [],//JSON.parse(window.localStorage.getItem('dollars') || '[]'),
            isLoading: true,
            errors: null
        }
        this.getDollars = this.getDollars.bind(this)
    }

    componentDidMount() {
        if (this.state.usd.length === 0) this.getDollars();
    }

    async getDollars(date) {
        this.setState({ errors: null });
        let url = Date ? buildDateURL(date) : buildDateURL();

        let BASE_URL = process.env.REACT_APP_BASE_URL;
        let API_KEY = process.env.REACT_APP_API_KEY;

        try {
            const response = await axios.get(`${BASE_URL}${url}?apikey=${API_KEY}&formato=json`);
            this.setState({ usd: response.data.Dolares, isLoading: false })
            //window.localStorage.setItem('dollars', JSON.stringify(response.data.Dolares))
        } catch (error) {
            let err = error.response.data;
            this.setState({ errors: err, isLoading: false });
        }

    }

    render() {
        const { usd, errors } = this.state;
        return (
            <div className='Main'>
                <div className="Main-sidebar">
                    <div>
                        <IconContext.Provider value={{ className: 'Main-sidebar-sign' }}>
                            <RiMoneyDollarCircleLine />
                        </IconContext.Provider>
                    </div>
                    <div>
                        <DatePicker getData={this.getDollars} />
                    </div>
                </div>
                <div className='Main-content'>
                    {(errors !== null)
                        ? (
                            <Error error={errors} />
                        )
                        : (
                            <div>
                                <Display max={getMax(usd)} min={getMin(usd)} avg={getAvg(usd)} />
                                {this.isLoading
                                    ?(<Spinner />)
                                :   (<Chart2 data={usd} />)}
                            </div>
                        )
                    }
                </div>
            </div>

        );
    }
}

export default Main;