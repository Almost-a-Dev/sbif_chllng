import React from 'react';
import { IconContext } from "react-icons";
import { TiArrowUpThick, TiArrowDownThick } from 'react-icons/ti';
import { FaRegMoneyBillAlt } from 'react-icons/fa';
import { MdMoneyOff } from 'react-icons/md';
import './Display.css'

const Display = ({ max, min, avg }) => {
    return (
        <div className='Display'>
            <div className='Display-title'>
            <IconContext.Provider value={{ className: 'Display-icons indicators-dollar' }}>
                <h3>Dollar Price <FaRegMoneyBillAlt /></h3>
            </IconContext.Provider>
            </div>
            <IconContext.Provider value={{ className: 'Display-icons indicators-up' }}>
                <h5>Valor maximo <TiArrowUpThick />{max} </h5>
            </IconContext.Provider>
            <IconContext.Provider value={{ className: 'Display-icons indicators-down' }}>
                <h5>Valor minimo <TiArrowDownThick />{min} </h5>
            </IconContext.Provider>
            <IconContext.Provider value={{ className: 'Display-icons indicators-average' }}>
                <h5>Valor promedio <MdMoneyOff />{avg}</h5>
            </IconContext.Provider>
        </div>
    );
}

export default Display;