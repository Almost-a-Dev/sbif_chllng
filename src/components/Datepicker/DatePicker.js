import React, { Component } from 'react';
import { getTimezone } from '../../utils/Utils';
import './DatePicker.css'

class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: getTimezone(),
            endDate: getTimezone()
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(evt) {
        evt.preventDefault();
        this.props.getData(this.state);
        this.setState({ startDate: getTimezone(), endDate: getTimezone() })
    }

    handleChange(evt) {
        this.setState({
            [evt.target.name]: evt.target.value
        })
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} className='Form'>
                <div className="Form-group">
                    <label htmlFor="startDate">Fecha Inicio: </label>
                    <input
                        type="date"
                        name="startDate"
                        id="startdate"
                        value={this.state.startDate}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="Form-group">
                    <label htmlFor="endDate">Fecha Termino: </label>
                    <input
                        type="date"
                        name="endDate"
                        id="endDate"
                        value={this.state.endDate}
                        onChange={this.handleChange}
                    />
                </div>
                <button>Buscar</button>
            </form>
        );
    }
}

export default DatePicker;