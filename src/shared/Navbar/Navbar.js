import React from 'react';
import './Navbar.css'
import logo from '../../cumplo.png'

const Navbar = () => {
    return (
        <div className="Navbar">
            <div className="Navbar-logo">
                <a href="https://secure.cumplo.cl/"><img src={logo} alt="Cumplo" /></a>
            </div>
            <div className="Navbar-links">
                <ul>
                    <li><a href="https://github.com/AlmostADev">About Me</a></li>
                </ul>
            </div>
        </div>
    )
}

export default Navbar;