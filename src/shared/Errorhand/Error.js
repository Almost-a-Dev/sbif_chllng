import React from 'react';
import { IconContext } from "react-icons";
import { MdErrorOutline } from 'react-icons/md';
import './Error.css'

const Error = ({ error }) => {
    return (
        <div className='Error'>
            <div>
                <IconContext.Provider value={{ className: 'Error-code' }}>
                    <MdErrorOutline />
                </IconContext.Provider>
                <div className='Error-info'>
                    <h1>{error.CodigoHTTP}</h1>
                    <h3>{error.Mensaje}</h3>
                </div>
            </div>
        </div>
    );
}

export default Error;