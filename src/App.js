import React from 'react';
import './App.css';
import Navbar from './shared/Navbar/Navbar';
import Main from './components/Main/Main';

function App() {
  return (
    <div>
      <Navbar />
      <div className="App">
        <Main />
      </div>
    </div>
  );
}

export default App;
